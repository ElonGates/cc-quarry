local args = { ... }
local turn_right = true
local width = assert(tonumber(args[1]), "Usage: bexcavate <Width> <Depth> <Length>")
local length = assert(tonumber(args[3]) - 1, "Usage: bexcavate <Width> <Depth> <Length>")
local depth = assert(tonumber(args[2]), "Usage: bexcavate <Width> <Depth> <Length>")

local function rotate_right()
    turtle.turnRight()
    turtle.turnRight()
end

local function forward()
    while not turtle.forward() do
        turtle.dig()
        turtle.attack()
        sleep()
    end
end

local function turn_around()
    if turn_right then
        turtle.turnRight()
        turtle.dig()
        forward()
        turtle.turnRight()
        turn_right = false
    else    
        turtle.turnLeft()
        turtle.dig()
        forward()
        turtle.turnLeft()
        turn_right = true      
    end
end

local function reset()
    turtle.turnRight()
    if turn_right then
        turtle.turnRight()
        for z = 1, length, 1 do
            forward()
        end
        turtle.turnRight()
    end
    for x = 1, width - 1, 1 do
        forward()
    end
    turtle.turnRight()
    turtle.digDown()
    turtle.down()
end


local function refuel()
    turtle.select(1)
    turtle.refuel()
end

print("Better Excavate by sunset and Sky.")
print("https://github.com/sunset-developer")
print("------------------------------------")
print("Excavation initiated, please monitor occasionally.")
for y = 1, depth do
    refuel()
    turn_right = true
    for x = 1, width, 1 do
        for z = 1, length, 1 do
            turtle.dig()
            forward()
        end
        if x < width then
            turn_around()
        else
            reset()
        end
    end
    print("Layer completed, " .. depth - y .." left to go.")
end
print("Excavation complete, enjoy :)")
